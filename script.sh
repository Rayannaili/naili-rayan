#!/bin/bash


# Copier le fichier de configuration sur le serveur

cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config

cp ./config__files/ssh/Banner /etc/Banner


echo "Veuillez mettre un nom d'utilisateur : \n"
read username 
adduser $username 
usermod -aG sudo $username 

echo "Veuillez entrer un mot de passe : \n"
passwd $username 

echo "entrer une clé public : \n" 
read KeySsh 
echo $KeySsh >> ~/.ssh/authorized_keys



# Redemarrer le service sshd 

systemctl restart sshd 

